<?php

include_once 'NamespaceA.php';

class AutoloaderClass
{
    protected array $namespaces;

    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    public function addNamespace(NamespaceA $namespace)
    {
        $this->namespaces[] = $namespace;
    }

    public function loadClass($class)
    {
        $class_as_array = explode("\\", $class);

        foreach ($this->namespaces as $namespace){

            $relative_class = $this->findPrefix($namespace->getPrefixAsArray(), $class_as_array);

            if ($relative_class === false) {
                continue;
            }

            $file = $namespace->getBaseDir()
                . DIRECTORY_SEPARATOR
                . implode(DIRECTORY_SEPARATOR, $relative_class)
                . '.php';

            if($this->requireFile($file)){
                return true;
            }
        }

        return false;
    }

    protected function findPrefix(array $prefix, array $class)
    {
        while (!empty($prefix)){
            if(array_shift($prefix) !==  array_shift($class)){
                return false;
            }
        }

        return $class;
    }

    protected function requireFile(string $file): bool
    {
        if (file_exists($file)) {
            require $file;
            return true;
        }
        return false;
    }

}