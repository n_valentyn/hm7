<?php

class NamespaceA
{
    private string $prefix;
    private string $base_dir;

    /**
     * Namespaces constructor.
     * @param string $prefix
     * @param string $base_dir
     */
    public function __construct(string $prefix, string $base_dir)
    {
        $this->setPrefix($prefix);
        $this->setBaseDir($base_dir);
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     */
    private function setPrefix(string $prefix): void
    {
        $this->prefix = trim($prefix, '\\');
    }

    /**
     * @return string
     */
    public function getBaseDir(): string
    {
        return $this->base_dir;
    }

    /**
     * @param string $base_dir
     */
    private function setBaseDir(string $base_dir): void
    {
        $this->base_dir = rtrim($base_dir, DIRECTORY_SEPARATOR);
    }

    public static function creat(string $prefix, string $base_dir)
    {
        return new NamespaceA($prefix, $base_dir);
    }

    public function getPrefixAsArray(): array
    {
        return explode("\\", $this->prefix);
    }
}