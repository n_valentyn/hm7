<?php

include_once 'src/AutoloaderClass.php';

$autoloader = new AutoloaderClass();
$autoloader->addNamespace(NamespaceA::creat('App\\Finance', '../src/Finance'));
$autoloader->register();
