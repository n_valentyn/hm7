<?php

namespace App\Finance;

use App\Finance\Currency;
use InvalidArgumentException;

class Money
{
    private float $amount;
    private Currency $currency;

    public function __construct(float $amount,Currency $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     */
    public function setCurrency(Currency $currency): void
    {
        $this->currency = $currency;
    }

    public function equals(Money $money): bool
    {
        if($this->getAmount() === $money->getAmount() && $this->getCurrency()->equals($money->getCurrency())){
            return true;
        }

        return false;
    }

    public function add(Money $money): void
    {
        if (!$this->getCurrency()->equals($money->getCurrency())){
            throw new InvalidArgumentException('Currency is different');
        }

        $this->amount += $money->getAmount();
        //$this->setAmount($this->getAmount() + $money->getAmount());
        //как правильнее со стороны ООП? или нет никакого принципиальной разницы...

        //return this; ???
    }
}