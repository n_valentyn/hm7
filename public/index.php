<?php

require_once '../vendor/autoload.php';

use App\Finance\Currency;
use App\Finance\Money;

echo 'Currency' . '<br>' . '<br>';

$currency1 = new Currency('USD');
$currency2 = new Currency('EUR');
$currency3 = new Currency('USD');

try {
    $currency4 = new Currency('US');
} catch (InvalidArgumentException $e) {
    echo $e->getMessage() . ' - in line: ' . $e->getLine();
    echo '<br>';
    echo '<br>';
}

echo '$currency1: ' . $currency1->getIsoCode() . '<br>';
echo '$currency2: ' . $currency2->getIsoCode() . '<br>';
echo '$currency3: ' . $currency3->getIsoCode() . '<br>';
echo '<br>';

echo '($currency1 === $currency2): ';
var_dump($currency1->equals($currency2));
echo '<br>';
echo '($currency1 === $currency3): ';
var_dump($currency1->equals($currency3));
echo '<br>';


echo '--------------------------';
echo '<br>';
echo 'Money' . '<br>' . '<br>';

$money1 = new Money(2.25, new Currency('USD'));
$money2 = new Money(1.5, new Currency('EUR'));
$money3 = new Money(2.75, new Currency('USD'));
$money4 = new Money(2.75, new Currency('USD'));
$money5 = new Money(2.75, new Currency('EUR'));

echo '$money1: ' . $money1->getAmount() . ' ' . $money1->getCurrency()->getIsoCode() . '<br>';
echo '$money2: ' . $money2->getAmount() . ' ' . $money2->getCurrency()->getIsoCode() . '<br>';
echo '$money3: ' . $money3->getAmount() . ' ' . $money3->getCurrency()->getIsoCode() . '<br>';
echo '$money4: ' . $money4->getAmount() . ' ' . $money4->getCurrency()->getIsoCode() . '<br>';
echo '$money5: ' . $money5->getAmount() . ' ' . $money5->getCurrency()->getIsoCode() . '<br>';
echo '<br>';

echo '($money1 === $money2): ';
var_dump($money1->equals($money2));
echo '<br>';
echo '($money1 === $money3): ';
var_dump($money1->equals($money3));
echo '<br>';
echo '($money3 === $money5): ';
var_dump($money3->equals($money5));
echo '<br>';
echo '($money3 === $money4): ';
var_dump($money3->equals($money4));
echo '<br>';
echo '<br>';

$money1->add($money3);
echo '($money1 + $money3): ' . $money1->getAmount() . ' ' . $money1->getCurrency()->getIsoCode() . '<br>';

try {
    $money1->add($money2);
} catch (InvalidArgumentException $e) {
    echo $e->getMessage() . ' - in line: ' . $e->getLine();
    echo '<br>';
    echo '<br>';
}